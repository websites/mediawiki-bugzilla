<?php

use MediaWiki\MediaWikiServices;

class BugzillaHooks
{
    /**
     * @param Parser $parser instance of Parser
     * @return bool true
     */
    public static function onParserFirstCallInit(Parser $parser)
    {
        $parser->setHook('bugzilla', Closure::fromCallable([self::class, 'renderTagBuzilla']));
    }

    /**
     * Callback function for the <bugzilla> parser hook.
     *
     * @param string $content (a JSON input)
     * @param array $attributes
     * @param Parser $parser
     * @param PPFrame $frame
     * @return array|string
     */
    public static function renderTagBuzilla(string $input, array $args, Parser $parser, PPFrame $frame): string
    {
        $class = "";
        if (isset($args['class'])) {
            $class = $args['class'];
        }

        $data = json_decode($input, true);
        $config = MediaWikiServices::getInstance()->getConfigFactory()->makeConfig('bugzilla');
        $url = $config->get('BugzillaRestApiUrl') . '/bug?';
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $value = implode(",", $data['include_fields']);
            }
            $url .= $key . '=' . urlencode($value) . '&';
        }
        $bugs = json_decode(file_get_contents($url), true);

        $string = '<table class="table table-bordered table-sm ' . $class . '">';

        if (isset($args['title'])) {
            $string .= '<caption style="caption-side: top">' . $args['title'] . (isset($args['debug']) ? $url : '' ) . '</caption>';
        }
        $string .= '<thead><tr>';
        foreach ($data['include_fields'] as $field) {
            $string .= '<td>' . $field . '</td>';
        }

        $string .= "</tr></thead><tbody>";

        foreach ($bugs['bugs'] as $bugs) {
            $string .= '<tr>';
            foreach ($data['include_fields'] as $field) {
                if ($field === 'id') {
                    $string .= '<td><a href="https://bugs.kde.org/show_bug.cgi?id=' . $bugs[$field] . '">' . $bugs[$field] . '</a></td>';
                } else {
                    $string .= '<td>' . $bugs[$field] . '</td>';
                }
            }
            $string .= '</tr>';
        }

        $string .= '</tbody></table>';

        return $string;
    }
}
